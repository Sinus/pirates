Na potrzeby niniejszego zadania przenosimy si� my�l� na Karaiby (albo do �wiata filmowego) i przygotowujemy si� do napisania gry opisuj�cej walki okr�t�w pirackich. Napiszemy jednak tylko bardzo podstawowy szkielet umo�liwiaj�cy kontrolowanie sk�adu pirackich band. 

Na potrzeby zadania ka�dy z okr�t�w posiada pewn� (sta��) liczb�:
* dzia�
* maszt�w
* wiose�
Dwa okr�ty posiadaj�ce dok�adnie takie samo oprzyrz�dowanie uwa�amy za nale��ce do tej samej klasy i identyczne, r�ni�ce si� liczb� czegokolwiek -- za r�ne. Okr�ty tej samej klasy mog� ��czy� si� w bandy.

Poniewa� piraci s� lud�mi rozs�dnymi, w przypadku konfliktu dw�ch band, banda posiadaj�ca okr�ty o gorszym sprz�cie poddaje si� i automatycznie staje si� �upem drugiej bandy. Sprz�t por�wnywany jest leksykograficznie, najpierw liczba dzia�, potem liczba maszt�w, a na ko�cu liczba wiose�.

Cele zadania s� dwa:
1. Stworzenie typu szablonowego ShipGear, reprezentuj�cego wyposa�enie statku. Nale�y nast�pnie stworzy� trzy klasy -- Cannon, Mast i Oar -- odpowiadaj�ce jednej sztuce ka�dego z typ�w sprz�tu.

Dla u�atwienia opisywania wyposa�enia definiujemy dodatkowo nast�puj�ce szablony:
template<class G1, class G2> struct add_gear;
Ta struktura powinna zawiera� publiczn� definicj� "type", stanowi�c� sum� wyposa�enia zawartego w G1 i G2.

template<class G1, class G2> struct remove_gear;
Ta struktura powinna zawiera� publiczn� definicj� "type", stanowi�c� wyposa�enie G1 pomniejszone o G2. Je�li kt�ra� z cech powinna by� w wyniku odejmowania (lub jakiejkolwiek innej operacji) ujemna, to wynosi ona 0. 

template<class G1, unsigned int N> struct multiply_gear;
Ta struktura powinna zawiera� publiczn� definicj� "type", stanowi�c� wyposa�enie zawarte w G1 powi�kszone N razy.

template<class G1, unsigned int N> struct split_gear;
Ta struktura powinna zawiera� publiczn� definicj� "type", stanowi�c� wyposa�enie zawarte w G1 podzielone na N r�wnych cz�ci (dzielimy oczywi�cie ca�kowitoliczbowo, ew. reszta z dzielenia przepada).

2. Stworzenie szablonu klasy Squad<Gear>, reprezentuj�cego band� okr�t�w. Jest on parametryzowany wyposa�eniem pojedynczego okr�tu w oddziale.

Klasa Squad<Gear> powinna udost�pnia� nast�puj�ce metody:

Squad();
Konstruktor domy�lny, banda o liczno�ci = 1

Squad(unsigned int const);

Squad(Squad<Gear> const &);

unsigned int get_count() const;
Metoda zwracaj�ca liczno�� bandy.

Klasa powinna zawiera� publiczn� definicj� typu reprezentuj�cego wyposa�enie, o nazwie gear_type oraz zmienn� statyczn� gear typu gear_type.

Nale�y stworzy� te� globalne funkcje:
Squad<JointGear> const join_ships(Squad<Gear> const&, Squad<OtherGear> const&)
Tworzy ona band� nowych, mocniejszych okr�t�w -- ka�da cecha stanowi sum� odpowiednich cech dw�ch starych band, a liczno�� bandy okre�lamy przez dost�pn� ilo�� sprz�tu.

Squad<SmallGear> const split_ships(Squad<Gear> const&)
Dzieli ona band� na dwie nowe o tej samej liczno�ci, z�o�one z mniejszych okr�t�w -- ka�da cecha stanowi iloraz (ca�kowitoliczbowy) starej warto�ci cechy przez 2; ew. reszta z dzielenia przepada.

oraz

expected_booty(Squad<Gear> const&, Squad<OtherGear> const&)

przekazuj�c� w wyniku obiekt reprezentuj�cy band�, kt�ra b�dzie oczekiwanym �upem w konflikcie dw�ch band. Wraz z t� funkcj� mo�na doda� wzorce pomocnicze, je�eli b�dzie taka potrzeba.

Dodatkowo powinny dzia�a�:
Operatory: +, -, +=, -= (na grupach statk�w; w przypadku osi�gni�cia ujemnej liczby statk�w, wynikiem jest zero).
Operatory: *, /, *=, /= (mno�enie/dzielenie przez liczb� ca�kowit� -- modyfikuje liczno�� grupy; liczno�� ma pozosta� ca�kowita i nieujemna, jak wy�ej).
Por�wnania: ==, !=, <, >, <=, >= (bandy o takim samym wyposa�eniu por�wnujemy standardowo (liczno�� bandy), w bandach o r�nych wyposa�eniu decyduje liczba dzia� NA POJEDYNCZYM STATKU).
Operator << -- wypisuje na strumie� opis bandy, w formacie (bez znaku ko�ca linii):
Ships: 20; Ship gear: Cannons: 3, Masts: 1, Oars: 0

Dodatkowe uwagi:
* Operatory powinny mie� konwencjonaln� semantyk�, tzn. by� przemienne i ��czne, gdy tylko jest to sensowne matematycznie.
* Nale�y d��y� do tego, aby b��dne u�ycia implementowanych typ�w danych by�y wykrywane jak najwcze�niej -- ju� na etapie kompilacji.

Przyk�ady u�ycia:

#include <typeinfo>
#include <cassert>
#include <iostream>

#include "shipwreck.hh"

typedef add_gear<Cannon, Mast>::type sloop;
typedef add_gear<multiply_gear<Cannon, 10>::type, multiply_gear<Oar, 20>::type>::type galley;

int main() {
    Squad<Mast> s1; // pojedynczy jednomasztowiec
    Squad<galley> s2(20); // banda 20 galer
    Squad<galley> s3(s2); // druga banda 20 galer
    Squad<Cannon> s4; // p�ywaj�ca armata

    // ��czymy sprz�t ,,na sucho''
    assert(typeid(Squad<add_gear<Cannon, Mast>::type>::gear_type) == typeid(sloop)); 

    // ��czymy statki
    assert(join_ships(s1, s4).get_count() == 1);
    assert(typeid(join_ships(s1, s4).gear) == typeid(sloop));
    
    // sumujemy bandy
    // assert((s1 + s2).get_count() == 21); // to si� nie powinno kompilowa�
    assert((s2 + s3).get_count() == 40);
    // assert((s3 += s4).get_count == 21); // to te� nie
    assert((s2 += s2).get_count() == 40);
    assert(s2.get_count() == 40);

    // rozpraszamy bandy (przerabiamy okr�ty na mniejsze)
    assert(s2 > s3);
    assert(typeid(split_ships(s3).gear) 
        == typeid(add_gear<multiply_gear<Cannon, 5>::type, 
                           multiply_gear<Oar, 10>::type>::type));
    assert(split_ships(s3).get_count() == 20);

    assert(typeid(split_ships(split_ships(s3)).gear) 
        == typeid(add_gear<multiply_gear<Cannon, 2>::type, 
                           multiply_gear<Oar, 5>::type>::type));
    assert(split_ships(split_ships(s3)).get_count() == 20);

    // odejmujemy bandy
    assert((s2 - s3).get_count() == 20);
    assert((s3 - s2).get_count() == 0); // sic!

    // wyznaczamy �up w przypadku konfliktu dw�ch band
    assert(typeid(expected_booty(s2, s4).gear) 
        == typeid(Cannon));
    assert(expected_booty(s2, s4).get_count() == 1);

    std::cout << "all ok!" << std::endl;
}