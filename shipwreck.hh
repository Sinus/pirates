#ifndef _SHIPWRECK
#define _SHIPWRECK


#include <iostream>
#include <typeinfo>
#include <type_traits>
#include <cassert>

namespace {

using std::cout;
using std::endl;
using std::min;
using std::ostream;
using std::enable_if;
    
    constexpr unsigned int diff (const unsigned int a, const unsigned int b) {
        return (a > b) ? a - b : 0;
    }

    const unsigned int calculate_quantity(unsigned int G1, 
            unsigned int G2, unsigned int joint) {
        //given both gears & destination gear calculate target quantity of gear
        //or max_int if it will not be present
        unsigned int max_int = 0 - 1;
        if (joint == 0)
            return max_int;
        else
            return (G1 + G2) / joint;
    }

    template <class G1, class G2>
    struct choose_booty {
        //true if G1 will be booty in expected_booty()
        static const bool val = ((G1::gear::cannon < G2::gear::cannon) ||
            (G1::gear::cannon == G2::gear::cannon &&
            G1::gear::mast < G2::gear::mast) ||
            (G1::gear::cannon == G2::gear::cannon &&
            G1::gear::mast == G2::gear::mast &&
            G1::gear::oar < G2::gear::oar));
        };
}

template <unsigned int c, unsigned int m, unsigned int o>
struct ShipGear {
    constexpr static auto cannon = c;
    constexpr static auto mast = m;
    constexpr static auto oar = o;
    typedef ShipGear<c, m, o> gear;
};

typedef ShipGear<1, 0, 0> Cannon;
typedef ShipGear<0, 1, 0> Mast;
typedef ShipGear<0, 0, 1> Oar;

template <class G1, const unsigned int N>
struct split_gear {
    static_assert(N != 0, "Dividing by 0\n");
    typedef ShipGear<G1::gear::cannon / N, 
            G1::gear::mast / N, 
            G1::gear::oar / N> type;
};

template <class G1, class G2>
struct add_gear {
    typedef ShipGear<G1::gear::cannon + G2::gear::cannon, 
            G1::gear::mast + G2::gear::mast, 
            G1::gear::oar + G2::gear::oar> type;
};

template <class G1, class G2>
struct remove_gear {
    typedef ShipGear<diff(G1::gear::cannon, G2::gear::cannon), 
            diff(G1::gear::mast, G2::gear::mast), 
            diff(G1::gear::oar, G2::gear::oar)> type;
};

template <class G1, unsigned int N>
struct multiply_gear {
    typedef ShipGear<G1::gear::cannon * N, G1::gear::mast * N,
            G1::gear::oar * N> type;
};

template <class Gear>
class Squad {
    private:
        unsigned int count;

    public:
        typedef Gear gear_type;
        static Gear gear;

        Squad() {
            count = 1;
        }

        explicit Squad(unsigned int const x) : count(x) {
            //explicit to make sure things like squad1 + 42 don't compile
            assert(x > 0);
        }

        Squad(Squad<Gear> const & x) : count(x.get_count()) {

        }

        unsigned int get_count() const {
            return count;
        }

        Squad<Gear>& operator+= (const Squad<Gear>& s) {
            this->count += s.count;
            return *this;
        }

        Squad<Gear>& operator-= (const Squad<Gear>& s) {
            this->count = diff(this->count, s.count);
            return *this;
        }

        Squad<Gear>& operator*= (const int& n) {
            if (n < 0)
                count = 0;
            else
                count *= n;
            return *this;
        }

        Squad<Gear>& operator/= (const int& n) {
            if (n < 0)
                count = 0;
            else
                count /= n;
            return *this;
        }

        const Squad<Gear> operator* (const int& n) const {
            Squad<Gear> result(*this);
            return result *= n;
        }

        const Squad<Gear> operator/ (const int& n) const {
            assert(n != 0);
            Squad<Gear> result(*this);
            return result /= n;
        }

        const Squad<Gear> operator+ (const Squad<Gear>& s) const {
            Squad<Gear> result(*this);
            return result += s;
        }

        const Squad<Gear> operator- (const Squad<Gear>& s) const {
            Squad<Gear> result(*this);
            return result -= s;
        }

        friend ostream& operator<< (ostream& out, const Squad<Gear>& s) {
            out << "Ships: " << s.count << "; Ship gear: Cannons: " << 
                Gear::cannon << ", Masts: " << Gear::mast << ", Oars: " <<
                Gear::oar;
            return out;
        }
};

template <typename G1, typename G2>
const bool operator== (const Squad<G1>& s1, const Squad<G2>& s2) {
    return (typeid(G1) == typeid(G2) && s1.get_count() == s2.get_count());
}

template <typename G1, typename G2>
const bool operator!= (const Squad<G1>& s1, const Squad<G2>& s2) {
    return !(s1 == s2);
}

template <typename G1, typename G2>
const bool operator>= (const Squad<G1>& s1, const Squad<G2>& s2) {
    if (typeid(G1) == typeid(G2))
        return s1.get_count() >= s2.get_count();
    else
        return G1::gear::cannon >= G2::gear::cannon;
}

template <typename G1, typename G2>
const bool operator> (const Squad<G1>& s1, const Squad<G2>& s2) {
    return (s1 >= s2 && s1 != s2);
}

template <typename G1, typename G2>
const bool operator<= (const Squad<G1>& s1, const Squad<G2>& s2) {
    return s2 >= s1;
}

template <typename G1, typename G2>
const bool operator< (const Squad<G1>& s1, const Squad<G2>& s2) {
    return s2 > s1;
}

template <typename G1>
const Squad<G1> operator* (const int& n, const Squad<G1>& s) {
    return s * n;
}

template <typename G1, typename G2> 
Squad<typename add_gear<G1, G2>::type> const join_ships
(const Squad<G1>& squad1, const Squad<G2>& squad2) {
    typedef typename add_gear<G1, G2>::type joint_gear;
    unsigned int number;
    //if the ship has no gear, quantity of new band will be sum of quantities
    if (joint_gear::cannon == 0 && joint_gear::mast == 0 &&
            joint_gear::oar == 0)
        number = squad1.get_count() + squad2.get_count(); 

    else {
        //there is something on the ship, so we calculate how many ships 
        //will there be
        unsigned int quant1, quant2, quant3;

        quant1 = calculate_quantity(G1::gear::cannon, G2::gear::cannon,
                joint_gear::cannon);

        quant2 = calculate_quantity(G1::gear::mast, G2::gear::mast,
                joint_gear::mast);

        quant3 = calculate_quantity(G1::gear::oar, G2::gear::oar,
                joint_gear::oar);

        number = min(quant3, min(quant2, quant1));
    }

    Squad<joint_gear> result(number);
    return result;
}

template <typename G1> 
Squad<typename split_gear<G1, 2>::type> const split_ships(const Squad<G1>& s) {
    typedef typename split_gear<G1, 2>::type small_gear;
    Squad<small_gear> result(s.get_count());
    return result;
}

template<class G1, class G2> //this runs only if G1 is booty
typename enable_if<choose_booty<G1, G2>::val, Squad<G1> >::type
expected_booty(Squad<G1>& s1, Squad<G2>& s2) {
    Squad<G1> result(s1);
    return result;
}

template<class G1, class G2> //this runs only if G2 is booty
typename enable_if<!choose_booty<G1, G2>::val, Squad<G2> >::type
expected_booty(Squad<G1>& s1, Squad<G2>& s2) {
    Squad<G2> result(s2);
    return result;
}
#endif
